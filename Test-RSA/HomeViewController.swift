//
//  HomeViewController.swift
//  Test-RSA
//
//  Created by Viphou on 5/13/18.
//  Copyright © 2018 MEF. All rights reserved.
//

import UIKit
import QRCodeReaderViewController
import AlamofireImage

class HomeViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    @IBOutlet weak var lbl: UILabel!
    
    @IBOutlet weak var img0: UIImageView!
    @IBOutlet weak var img1: UIImageView!
    
    let imagePicker = UIImagePickerController()
    
    @IBOutlet weak var datePicker: UIDatePicker!
    
    //
    let dateFormatter = DateFormatter()
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        //
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        //
        let str = NSLocalizedString("WELCOME", comment: "startApp")
        lbl.text = str
        // Do any additional setup after loading the view.
        if Tool.getImageByName(name: "x-women.jpg") == nil{
            img1.backgroundColor = .red
        }
        else{
            img1.image = Tool.getImageByName(name: "x-women.jpg")
        }
        //
        let url = URL(string: "https://pmcvariety.files.wordpress.com/2015/01/rose-byrne-x-men.jpg")
        img0.af_setImage(withURL: url!, placeholderImage: nil, filter: nil, progress: nil, progressQueue: DispatchQueue.global(), imageTransition: UIImageView.ImageTransition.noTransition, runImageTransitionIfCached: false) { (imageResult) in
            print("Completeed load image !!!")
            //
            Tool.saveImage(img: self.img0.image!, name: "x-women.jpg")
            //
            self.img1.image = self.img0.image
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        UIView.animate(withDuration: 5, animations: {
            self.img0.frame = self.img1.frame
        }) { (comleted) in
            self.view.backgroundColor = .green
            self.datePicker.isHidden = false
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnScanQRClicked(_ sender: Any) {
        
        let reader = QRCodeReader(metadataObjectTypes: [AVMetadataObject.ObjectType.qr])
        
        let vc = QRCodeReaderViewController(cancelButtonTitle: "CANCEL", codeReader: reader, startScanningAtLoad: true, showSwitchCameraButton: true, showTorchButton: true)
        
        reader.setCompletionWith { (resultString) in
            //Dismiss Scanner
            vc.dismiss(animated: true, completion: {
                self.lbl.text = resultString
                //Open URL via Safari
                let url = URL(string: resultString!)
                if url != nil && UIApplication.shared.canOpenURL(url!){
                    UIApplication.shared.open(url!, options: [:], completionHandler: { (completed) in
                        
                    })
                }
                else{
                    let alerController = UIAlertController(title: "Result", message: resultString, preferredStyle: .alert)
                    let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (action) in
                        
                    })
                    
                    alerController.addAction(cancelAction)
                    
                    self.present(alerController, animated: true, completion: {
                        
                    })
                }
                
            })
            
        }
        
        present(vc, animated: true) {
            
        }
        
    }

    @IBAction func btnLibClicked(_ sender: Any) {
        imagePicker.sourceType = .photoLibrary
        present(imagePicker, animated: true) {
            
        }
    }
    
    @IBAction func btnCamClicked(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
            imagePicker.sourceType = .camera
            imagePicker.cameraCaptureMode = .photo
            imagePicker.modalPresentationStyle = .fullScreen
            present(imagePicker, animated: true) {
                
            }
        }
        else{
            let alert = UIAlertController(title: "Camera not available", message: "Your device is not support wiht camera", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
                
            }
            alert.addAction(cancelAction)
            present(alert, animated: true) {
                
            }
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        let chosenImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        
        if chosenImage != nil{
            img1.image = chosenImage
        }
        
        picker.dismiss(animated: true) {
            
        }
    }
    
    @IBAction func datePickerChange(_ sender: Any) {
        
        dateFormatter.dateFormat = "dd-MMM-YYYY"
        
        let str = dateFormatter.string(from: datePicker.date)
        
        lbl.text = str
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
