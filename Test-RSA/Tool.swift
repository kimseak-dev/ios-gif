//
//  Tool.swift
//  KQUBE JOB
//
//  Created by Viphou Sam on 8/26/15.
//  Copyright (c) 2015 CINet. All rights reserved.
//

import UIKit

class Tool: NSObject {
    
    class func getBlueFooterColor()->(UIColor) {
        
        return(UIColor(red: 73/255, green: 167/255, blue: 237/255, alpha: 1))
        
    }
    
    class func getGreenFooterColor()->(UIColor) {
        
        return(UIColor(red: 136/255, green: 184/255, blue: 75/255, alpha: 1))
        
    }
    
    class func getRedFooterColor()->(UIColor) {
        
        return(UIColor(red: 221/255, green: 63/255, blue: 49/255, alpha: 1))
        
    }
    
    class func getGrayFooterColor()->(UIColor) {
        
        return(UIColor(red: 66/255, green: 90/255, blue: 107/255, alpha: 1))
        
    }
    
    class func getYellowGreenFooterColor()->(UIColor) {
        
        return(UIColor(red: 157/255, green: 156/255, blue: 65/255, alpha: 1))
        
    }
    //
    class func stringToCurrency(str:String)->(NSDecimalNumber){
        var currency = NSDecimalNumber(string: "0.00")
        if str.count > 0{
            let balance = Float(str)
            let strBalance = String(format: "%.2f", balance!)
            currency = NSDecimalNumber(string: strBalance)
        }
        return currency
    }
    class func stringToCurrencyString(str:String)->(String){
        var strBalance = "0.00"
        if str.count > 0{
            let balance = Float(str)
            if balance != nil{
                strBalance = String(format: "%.2f", balance!)
            }            
        }
        
        return strBalance
    }
    //
    class func getBlueNaviBarColor()->(UIColor) {
        
        return(UIColor(red: 41/255, green: 101/255, blue: 139/255, alpha: 1))
        
    }
    //
    class func getFemaleBgColor()->(UIColor) {

        return(UIColor(red: 186/255, green: 50/255, blue: 141/255, alpha: 1))
        
    }
    
    class func getMaleBgColor()->(UIColor) {
 
        return(UIColor(red: 76/255, green: 150/255, blue: 213/255, alpha: 1))
        
    }

    class func getPointIDColor()->(UIColor){
        return(UIColor(red: 114/255, green: 176/255, blue: 236/255, alpha: 1))
    }
    
    class func getTitleBlueColor()->(UIColor){
        return(UIColor(red: 114/255, green: 176/255, blue: 236/255, alpha: 1))
    }
    
    // Font
    
    class func getKhmerDangrekFont(size:CGFloat)->(UIFont) {

        return UIFont(name: "Dangrek", size: size)!
    }
    
    class func saveImage(img:UIImage, name:String){
        
        if let data = UIImagePNGRepresentation(img) {
            let filename = getDocumentsDirectory().appendingPathComponent(name)
            print("to save filename : \(filename.absoluteString)")
            try? data.write(to: filename)
        }
        
    }
    
    class func getImageByName(name:String)->(UIImage?){
        
        let filename = getDocumentsDirectory().appendingPathComponent(name)
        print("to get filename : \(filename.absoluteString)")
        let image = UIImage(contentsOfFile: filename.path)
        
        return image
        
    }
    
    class func deleteImageByName(name:String)->(Bool){
        
        var isSuccess = true
        
        let filename = getDocumentsDirectory().appendingPathComponent(name)
        
        let fileManager = FileManager.default
        if fileManager.isDeletableFile(atPath: filename.absoluteString){
            do {
                try fileManager.removeItem(at: filename)
            }
            catch{
                isSuccess = false
            }
        }
        
        return isSuccess
        
    }
    
    class func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        print("document directory : \(documentsDirectory)")
        return documentsDirectory
    }
    
    class  func convertToKhmerNumber(_ originalNumber:String)->(String){
        
        var khmerNumber = originalNumber.replacingOccurrences(of: "0", with: "០")
        khmerNumber = khmerNumber.replacingOccurrences(of: "1", with: "១")
        khmerNumber = khmerNumber.replacingOccurrences(of: "2", with: "២")
        khmerNumber = khmerNumber.replacingOccurrences(of: "3", with: "៣")
        khmerNumber = khmerNumber.replacingOccurrences(of: "4", with: "៤")
        khmerNumber = khmerNumber.replacingOccurrences(of: "5", with: "៥")
        khmerNumber = khmerNumber.replacingOccurrences(of: "6", with: "៦")
        khmerNumber = khmerNumber.replacingOccurrences(of: "7", with: "៧")
        khmerNumber = khmerNumber.replacingOccurrences(of: "8", with: "៨")
        khmerNumber = khmerNumber.replacingOccurrences(of: "9", with: "៩")
        
        return khmerNumber
    }
    
}
